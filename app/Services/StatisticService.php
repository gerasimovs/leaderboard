<?php

namespace App\Services;

use App\Game;

class StatisticService
{
    const DEFAULT_PERIOD = 'all';

    public function scoreCalculate($data)
    {
        $bonus = 13 * $data['kills'] + 3 * $data['headshots'];
        $penalty = $data['team_kills'] + 2 * $data['deaths'];

        return $bonus - $penalty;
    }

    public function getPeriods()
    {
        return [
            'today' => __('today'),
            'month' => __('per month'),
            'all'   => __('all time'),
        ];
    }

    public function getCurrentPeriodKey()
    {
        $periods = $this->getPeriods();
        $requestPeriod = request()->get('period');

        return isset($periods[$requestPeriod])
            ? $requestPeriod
            : self::DEFAULT_PERIOD;
    }

    public function getCurrentPeriod()
    {
        $periods = $this->getPeriods();

        return $periods[$this->getCurrentPeriodKey()];
    }

    public function isCurrent($periodKey)
    {
        return $periodKey === $this->getCurrentPeriodKey();
    }
}
