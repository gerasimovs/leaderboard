<?php

namespace App\Services;

use App\Player;

class PlayerService
{
    /**
     * Order by statistic and add to select totals
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function orderByTotals()
    {
        return Player::query()->select('players.*', 'sessions.id as session_id')
            ->selectRaw('SUM(statistic.kills)       as total_kills')
            ->selectRaw('SUM(statistic.headshots)   as total_headshots')
            ->selectRaw('SUM(statistic.team_kills)  as total_team_kills')
            ->selectRaw('SUM(statistic.deaths)      as total_deaths')
            ->selectRaw('SUM(statistic.score)       as total_score')
            ->join('statistic', 'players.id',       '=', 'statistic.player_id')
            ->join('sessions',  'sessions.id',      '=', 'statistic.session_id')
            ->join('teams',     'teams.id',         '=', 'players.team_id')
            ->groupBy('players.id', 'sessions.id')
            ->orderByRaw('SUM(statistic.score) DESC');
    }
}
