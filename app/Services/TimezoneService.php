<?php

namespace App\Services;

use DateTime;
use DateTimeZone;

class TimezoneService
{
    public function getForUser($time, \App\User $user)
    {
        $action = $user->actions->firstWhere('slug', 'sessions.has_location');
        $slug = $action
            ? json_decode($action->permission->value)
            : null;
        $location = $slug
            ? \App\Location::query()->where('slug', $slug)->first()
            : null;

        $timezone = $location->timezone ?? 'UTC';
        return $this->getDisplayDateTimeWithTimezone($time, $timezone);
    }

    public function getForSession($time, \App\Session $sessions)
    {
        $location = $sessions->location;
        $timezone = $location->timezone ?? 'UTC';
        return $this->getDisplayDateTimeWithTimezone($time, $timezone);
    }

    public function getList()
    {
        $idents = DateTimeZone::listIdentifiers();

        $data = $offset = $added = [];
        foreach (DateTimeZone::listAbbreviations() as $abbr => $info) {
            foreach ($info as $zone) {
                if (!empty($zone['timezone_id'])
                    && !in_array($zone['timezone_id'], $added)
                    && in_array($zone['timezone_id'], $idents)
                ) {
                    $offset[] = $zone['offset'] = $this->getOffset($zone['timezone_id']);
                    $added[] = $zone['timezone_id'];
                    $data[] = $zone;
                }
            }
        }

        array_multisort($offset, SORT_ASC, $data);
        $options = array();

        foreach ($data as $key => $row) {
            $options[$row['timezone_id']] = $this->getDisplay($row['timezone_id']);
        }

        return $options;
    }

    public function getDisplay($timezone)
    {
        $offset = $this->formatOffset(
            $this->getOffset($timezone)
        );

        return "{$offset} {$timezone}";
    }

    private function getDisplayDateTimeWithTimezone($time, $timezone)
    {
        $dateTime = new DateTime($time);
        $displayTime = $dateTime->setTimezone(new DateTimeZone($timezone))->format('Y-m-d H:i:s');
        $displayTz = $this->getDisplay($timezone);

        return "{$displayTime} {$displayTz}";
    }

    private function getOffset($timezoneId)
    {
        $timezone = new DateTimeZone($timezoneId);
        $currenTime = new DateTime(null, $timezone);
        return $timezone->getOffset($currenTime);
    }

    private function formatOffset($offset)
    {
        $hours = $offset / 3600;
        $remainder = $offset % 3600;
        $sign = $hours > 0 ? '+' : '-';
        $hour = (int) abs($hours);
        $minutes = (int) abs($remainder / 60);

        if ($hour == 0 AND $minutes == 0) {
            $sign = ' ';
        }

        return 'GMT' . $sign . str_pad($hour, 2, '0', STR_PAD_LEFT)
                .':'. str_pad($minutes,2, '0');
    }
}
