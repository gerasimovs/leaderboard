<?php

namespace App\Services;

use App\Location;

class LocationService
{
    public function getActive()
    {
        static $items;

        if ($items === null) {
            $items = Location::query()->where('is_active', true)->get();
        }

        return $items;
    }

    public function getWithSite()
    {
        static $items;

        if ($items === null) {
            $items = Location::query()->whereNotNull('url')->where('url', '<>', '')->get();
        }

        return $items;
    }
}
