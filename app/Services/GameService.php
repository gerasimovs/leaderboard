<?php

namespace App\Services;

use App\Game;

class GameService
{
    protected $items;

    public function getActive()
    {
        if ($this->items === null) {
            $this->items = Game::query()->where('is_active', true)->get();
        }

        return $this->items;
    }
}
