<?php

namespace App\Services\Admin;

class AsideService
{
    protected $items;

    public function __construct()
    {
        $this->setItems((array) config('admin.menu'));
    }

    public function setItems(array $items)
    {
        array_walk($items, [$this, 'modify']);
        $this->items = $items;
    }

    public function modify(&$item)
    {
        if (isset($item['permission']) && $item['is_show'] !== false) {
            $item['is_show'] = auth()->user()->can(...$item['permission']);
        }

        if (isset($item['title'])) {
            $item['title'] = __($item['title']);
        }

        if (isset($item['children'])) {
            array_walk($item['children'], [$this, 'modify']);
        }
    }

    public function getItems()
    {
        return $this->items;
    }

    public function setActive($route = null)
    {
        foreach ($this->items as &$item) {
            $this->setActiveItem($item, $route);
        }

        return $this;
    }

    private function setActiveItem(&$item, $route)
    {
        $item['active'] = $this->isActive($item, $route);
        $item['has_active'] = false;

        if (isset($item['children'])) {
            $item['children'] = (array) $item['children'];

            foreach ($item['children'] as &$subItem) {
                if ($this->setActiveItem($subItem, $route)) {
                    $item['has_active'] = true;
                }
            }
        }

        return $item['active'] || $item['has_active'];
    }

    private function isActive($item, $route)
    {
        return (isset($item['route']) && in_array($route, $item['route']))
            || (isset($item['aliases']) && in_array($route, $item['aliases']));
    }


}
