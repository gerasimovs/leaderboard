<?php

namespace App\Services\Admin;

use App\Action;

class ActionService
{
	private $items;

    public function getItems()
    {
        if ($this->items === null) {
            $this->items = Action::all();
        }

        return $this->items;
    }
}
