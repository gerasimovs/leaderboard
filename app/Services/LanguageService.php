<?php

namespace App\Services;

use App\Location;

class LanguageService
{
    protected $items;

    public function getCurrent()
    {
        return $this->getItems()->firstWhere('is_active', true)
            ?? $this->getItems()->firstWhere('abbr', config('app.fallback_locale'));
    }

    public function getItems()
    {
        if ($this->items === null) {
            $current = app()->getLocale();

            $this->items = collect(config('admin.languages'))
                ->where('is_show', true)->map(function ($language) use ($current) {
                    $language['is_active'] = $language['abbr'] === $current;
                    return $language;
                });
        }

        return $this->items;
    }
}
