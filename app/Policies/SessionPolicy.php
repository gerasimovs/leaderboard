<?php

namespace App\Policies;

use App\User;
use App\Session;
use Illuminate\Auth\Access\HandlesAuthorization;

class SessionPolicy
{
    use HandlesAuthorization;

    /**
     * @param User $user
     * @param string $ability
     * @return mixed
     */
    public function before(User $user, $ability)
    {
        $action = $user->actions->firstWhere('slug', 'sessions.all');

        if ($action && json_decode($action->permission->value) === true) {
            return true;
        }
    }

    /**
     * @param User $user
     * @param string $action
     * @return mixed
     */
    public function index(User $user, $current = null)
    {
        $action = $user->actions->firstWhere('slug', 'sessions.has_location');

        if ($action) {
            $value = json_decode($action->permission->value);
            return $current
                ? $value === $current
                : (bool) $value;
        }

        return false;
    }

    /**
     * Determine whether the user can view any sessions.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $this->index($user);
    }

    /**
     * Determine whether the user can view the session.
     *
     * @param  \App\User  $user
     * @param  \App\Session  $session
     * @return mixed
     */
    public function view(User $user, Session $session)
    {
        return $this->index($user, $session->location->slug ?? null);
    }

    /**
     * Determine whether the user can create sessions.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $this->index($user);
    }

    /**
     * Determine whether the user can update the session.
     *
     * @param  \App\User  $user
     * @param  \App\Session  $session
     * @return mixed
     */
    public function update(User $user, Session $session)
    {
        return $this->index($user, $session->location->slug ?? null);
    }

    /**
     * Determine whether the user can delete the session.
     *
     * @param  \App\User  $user
     * @param  \App\Session  $session
     * @return mixed
     */
    public function delete(User $user, Session $session)
    {
        return $this->index($user, $session->location->slug ?? null);
    }

    /**
     * Determine whether the user can restore the session.
     *
     * @param  \App\User  $user
     * @param  \App\Session  $session
     * @return mixed
     */
    public function restore(User $user, Session $session)
    {
        return $this->index($user, $session->location->slug ?? null);
    }

    /**
     * Determine whether the user can permanently delete the session.
     *
     * @param  \App\User  $user
     * @param  \App\Session  $session
     * @return mixed
     */
    public function forceDelete(User $user, Session $session)
    {
        return $this->index($user, $session->location->slug ?? null);
    }
}
