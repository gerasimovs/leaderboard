<?php

namespace App\Http\Middleware;

use Closure;

class SetLanguage
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($lang = $this->getLanguageFromRequest($request)) {
            app()->setLocale($lang);
            return $next($request)->withCookie(cookie()->forever('lang', $lang));
        }

        if ($lang = $request->cookie('lang')) {
            app()->setLocale($lang);
        }

        return $next($request);
    }

    private function getLanguageFromRequest($request)
    {
        $lang = $request->get('lang');
        return $lang && app(\App\Services\LanguageService::class)
            ->getItems()
            ->firstWhere('abbr', $lang)
                ? $lang
                : null;
    }
}
