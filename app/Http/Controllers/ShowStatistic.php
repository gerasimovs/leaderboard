<?php

namespace App\Http\Controllers;

use App\Game;
use App\Location;
use App\Player;
use App\Session;
use App\Statistic;
use App\Team;
use Illuminate\Http\Request;
use Illuminate\Support\Fluent;

class ShowStatistic extends Controller
{
    protected $showPlace = true;

    protected $showOnlyToday = true;

    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request, Location $location = null, Game $game = null)
    {
        if ($game === null) {
            $game = Game::query()->where('slug', config('admin.defaults.game'))->firstOrFail();
        }

        $sessions = Session::orderByTotals()
            ->with('players', 'team', 'location')
            ->where('sessions.game_id', $game->id)
            ->when($location, function ($query, $location) {
                $query->where('location_id', $location->id);
            })
            ->when($request->get('period'), function ($query, $period) {
                switch ($period) {
                    case 'all':
                        break;

                    case 'month':
                        $query->whereDate('sessions.played_at', '>', now()->subDay(30));
                        break;

                    case 'today':
                        $query->whereDate('sessions.played_at', '>=', now()->startOfDay());
                        break;
                }
            })
            ->when($request->get('query'), function ($query, $search) {
                $query->whereRaw('LOWER(teams.title) LIKE ?', ['%' . mb_strtolower($search) . '%']);
                $this->showPlace = false;
            })
            ->simplePaginate(100)
            ->appends(['query' => $request->get('query')]);

        $title = $location
            ? __('Game rating') . ' ' . $location->title
            : __('World rating');

        return view('ratings.index', [
            'title' => $title,
            'routes' => [
                'world' => [
                    'name' => 'home',
                    'title' => __('Open world rating'),
                ],
                'location' => [
                    'name' => 'locations.ratings.show',
                    'title' => __('Open city rating'),
                ],
                'alternative' => [
                    'name' => $location ? 'players.ratings.location' : 'players.ratings.world',
                    'title' => __('Show personal rating'),
                ],
            ],
            'location' => $location,
            'game' => $game,
            'sessions' => $sessions,
            'ratingEntity' => 'teams',
            'showPlace' => $this->showPlace,
        ]);
    }
}
