<?php

namespace App\Http\Controllers;

use App\Game;
use App\Location;
use App\Services\PlayerService;
use Illuminate\Http\Request;

class ShowPlayersStatistic extends Controller
{
    protected $showPlace = true;

    /**
     * Handle the incoming request.
     *
     * @param  Request $request
     * @param  Location|null $location
     * @return \Illuminate\Contracts\View\View
     */
    public function __invoke(Request $request, Location $location = null)
    {
        $game = Game::query()->where('slug', config('admin.defaults.game'))->firstOrFail();

        $players = app(PlayerService::class)->orderByTotals()
            ->addSelect('players.name AS title')
            ->with('session', 'team')
            ->when($location, function ($query, $location) {
                $query->where('sessions.location_id', $location->id);
            })
            ->when($request->get('period'), function ($query, $period) {
                switch ($period) {
                    case 'all':
                        break;

                    case 'month':
                        $query->whereDate('sessions.played_at', '>', now()->subDays(30));
                        break;

                    case 'today':
                        $query->whereDate('sessions.played_at', '>=', now()->startOfDay());
                        break;
                }
            })
            ->when($request->get('query'), function ($query, $search) {
                $query->whereRaw('LOWER(players.name) LIKE ?', ['%' . mb_strtolower($search) . '%']);
                $this->showPlace = false;
            })
            ->simplePaginate(100)
            ->appends(['query' => $request->get('query')]);

        $players->each(function ($player) {
            $player->id = $player->session_id;
            $player->setRelation('location', $player->session->location);
        });

        $title = $location
            ? __('Best players of') . ' ' . $location->title
            : __('World player rating');

        return view('ratings.index', [
            'title' => $title,
            'routes' => [
                'world' => [
                    'name' => 'players.ratings.world',
                    'title' => __('Open world rating'),
                ],
                'location' => [
                    'name' => 'players.ratings.location',
                    'title' => __('Open city rating'),
                ],
                'alternative' => [
                    'name' => $location ? 'locations.ratings.show' : 'home',
                    'title' => __('Show team rating'),
                ],
            ],
            'location' => $location,
            'game' => $game,
            'sessions' => $players,
            'ratingEntity' => 'players',
            'showPlace' => $this->showPlace,
        ]);
    }
}
