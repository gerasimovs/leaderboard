<?php

namespace App\Http\Controllers\Api;

use App\Game;
use App\Location;
use App\Session;
use App\Services\PlayerService;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Validator;

class GetPlayer extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $data = $this->requestValidate($request->all());
        $data['game'] = $data['game'] ?? config('admin.defaults.game');

        $game = Game::query()->where('slug', $data['game'])->firstOrFail();
        $location = isset($data['location'])
            ? Location::query()->where('slug', $data['location'])->firstOrFail()
            : null;

        $players = app(PlayerService::class)->orderByTotals()
            ->with('session.location', 'team')
            ->when($location, function ($query, $location) {
                $query->where('sessions.location_id', $location->id);
            })
            ->where(function ($query) use ($data) {
                $query->whereRaw('LOWER(players.name) LIKE ?', ['%' . mb_strtolower($data['query']) . '%'])
                    ->orWhere('players.name', 'like', '%' . $data['query'] . '%');
            })
            ->simplePaginate(100)
            ->appends(['query' => $request->get('query')]);

        $data = $players->map(function ($player) use ($game) {
            return [
                'title' => $player->name ?? null,
                'url' => route('sessions.show', ['game' => $game, 'session' => $player->session]),
                'location' => $player->session->location->title ?? null,
            ] + Arr::only($player->getAttributes(), [
                'total_deaths',
                'total_headshots',
                'total_kills',
                'total_score',
                'total_team_kills',
            ]);
        });

        return response()->json([
            'success' => true,
            'data' => $data,
        ]);
    }

    private function requestValidate($data)
    {
        return Validator::make($data, [
            'query' => 'required|string',
            'location' => 'nullable|exists:locations,slug',
            'game' => 'nullable|exists:games,slug',
        ])->validate();
    }
}
