<?php

namespace App\Http\Controllers\Api;

use App\Game;
use App\Location;
use App\Player;
use App\Session;
use App\Team;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;

class StoreStatistic extends Controller
{
    private $user;

    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request, Game $game = null)
    {
        if ($game === null) {
            $game = Game::query()->where('slug', config('admin.defaults.game'))->firstOrFail();
        }

        $this->user = $request->user();

        $requestData = $request->all();
        $requestData['location'] = $this->getLocation($request->get('location'));
        $requestData['date'] = $this->getDate($request->get('date'));
        $requestData['time'] = $request->get('time');
        $data = $this->requestValidate($requestData);

        $location = Location::query()->where('slug', $data['location'])->first();
        $session = $location->sessions()->firstOrNew([
            'game_id' => $game->id,
            'played_at' => date('Y-m-d H:i:s', strtotime($data['date'])),
            'time' => $this->getSessionSeconds($data['time']),
        ]);

        if (!$session->exists) {
            $session->save();

            $team = Team::query()->make([
                'title' => $data['team'],
            ]);
        } else {
            $team = Team::query()
                ->whereHas('players.sessions', function ($query) use ($session) {
                    $query->whereKey($session->getKey());
                })
                ->firstOrNew([
                    'title' => $data['team']
                ]);
        }

        if (!$team->exists) {
            $team->save();

            $statisticService = app(\App\Services\StatisticService::class);

            foreach ($data['players'] as $dataPlayer) {
                $player = $team->players()->create([
                    'name' => $dataPlayer['name'],
                ]);

                $statistics = [
                    'session_id' => $session->id,
                    'kills' => $dataPlayer['kills'],
                    'deaths' => $dataPlayer['deaths'],
                    'team_kills' => $dataPlayer['teamKills'],
                    'headshots' => $dataPlayer['headshots'],
                ];

                $statistics['score'] = $statisticService->scoreCalculate($statistics);

                $player->sessions()->sync([
                    $session->id => $statistics
                ], false);
            }
        }

        return response()->json([
            'success' => true,
            'data' => [
                'url' => route('sessions.show', ['game' => $game, 'session' => $session]),
                'adminurl' => route('admin.sessions.edit', ['session' => $session]),
            ],
        ]);
    }

    private function requestValidate($data)
    {
        return Validator::make($data, [
            'team' => 'required',
            'location' => 'required|exists:locations,slug',
            'date' => [
                'required',
                function ($attribute, $value, $fail) {
                    if (strtotime($value) === false) {
                        $fail(__(
                            'validation.date',
                            ['attribute' => __("validation.attributes.{$attribute}")]
                        ));
                    }
                },
            ],
            'time' => 'nullable|regex:/^\d+:\d{2}$/',
            'players' => 'required|array',
            'players.*.name' => 'required',
            'players.*.kills' => 'required|integer',
            'players.*.deaths' => 'required|integer',
            'players.*.teamKills' => 'required|integer',
            'players.*.headshots' => 'required|integer',
        ])->validate();
    }

    private function getSessionSeconds(?string $time)
    {
        if ($time === null) {
            return null;
        }

        $minutes = (int) Str::before($time, ':');
        $seconds = (int) Str::after($time, ':');
        return $minutes * 60 + $seconds;
    }

    private function getLocation(?string $inputLocation)
    {
        if ($inputLocation) {
            return strtolower($inputLocation);
        }

        $action = $this->user->actions->firstWhere('slug', 'sessions.has_location');

        return $action
            ? json_decode($action->permission->value)
            : null;
    }

    public function getDate(?string $inputDate)
    {
        return $inputDate
            ? \Carbon\Carbon::createFromFormat('!d.m.y', $inputDate)->format('Y-m-d H:i:s')
            : \Carbon\Carbon::now()->format('Y-m-d H:i:s');
    }
}
