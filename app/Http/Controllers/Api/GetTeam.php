<?php

namespace App\Http\Controllers\Api;

use App\Game;
use App\Location;
use App\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Validator;

class GetTeam extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $data = $this->requestValidate($request->all());
        $data['game'] = $data['game'] ?? config('admin.defaults.game');

        $game = Game::query()->where('slug', $data['game'])->firstOrFail();
        $location = isset($data['location'])
            ? Location::query()->where('slug', $data['location'])->firstOrFail()
            : null;

        $sessions = Session::orderByTotals()
            ->with('players', 'team')
            ->where('sessions.game_id', $game->id)
            ->when($location, function ($query, $location) {
                $query->where('location_id', $location->id);
            })
            ->where(function ($query) use ($data) {
                $query->whereRaw('LOWER(teams.title) LIKE ?', ['%' . mb_strtolower($data['query']) . '%'])
                    ->orWhere('teams.title', 'like', '%' . $data['query'] . '%');
            })
            ->simplePaginate(100)
            ->appends($data);

        $teams = $sessions->map(function ($session) use ($game) {
            return [
                'title' => $session->team->title ?? null,
                'url' => route('sessions.show', ['game' => $game, 'session' => $session]),
                'location' => $session->location->title ?? null,
                'adminurl' => route('admin.sessions.edit', ['session' => $session]),
            ] + Arr::only($session->getAttributes(), [
                'total_deaths',
                'total_headshots',
                'total_kills',
                'total_score',
                'total_team_kills',
            ]);
        });

        return response()->json([
            'success' => true,
            'data' => $teams,
        ]);
    }

    private function requestValidate($data)
    {
        return Validator::make($data, [
            'query' => 'required|string',
            'location' => 'nullable|exists:locations,slug',
            'game' => 'nullable|exists:games,slug',
        ])->validate();
    }
}
