<?php

namespace App\Http\Controllers\Api;

use App\Game;

class GetGame extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @return \Illuminate\Http\Response
     */
    public function __invoke()
    {
        $games = Game::query()->get();

        return response()->json([
            'success' => true,
            'data' => $games,
        ]);
    }
}
