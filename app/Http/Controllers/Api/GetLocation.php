<?php

namespace App\Http\Controllers\Api;

use App\Location;

class GetLocation extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @return \Illuminate\Http\Response
     */
    public function __invoke()
    {
        $locations = Location::query()->get();
        $nullLocation = app(Location::class)->forceFill([
            'is_active' => true,
            'slug' => null,
            'title' => '-',
        ]);
        $locations->prepend($nullLocation);

        return response()->json([
            'success' => true,
            'data' => $locations,
        ]);
    }
}
