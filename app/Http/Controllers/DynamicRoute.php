<?php

namespace App\Http\Controllers;

use App\Game;
use App\Location;
use Illuminate\Http\Request;

class DynamicRoute extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  Request $request
     * @param  string|null $path
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request, string $path = null)
    {
        $locationSlug = null;
        $gameSlug = null;

        if ($path) {
            $path = explode('/', $path);

            if (count($path) > 1) {
                [$locationSlug, $gameSlug] = $path;
            } elseif (count($path)) {
                [$gameSlug] = $path;
            } else {
                abort(404);
            }
        }

        $location = $locationSlug ? Location::query()->where('slug', $locationSlug)->firstOrFail() : null;
        $game = Game::query()->where('slug', $gameSlug ?: config('admin.defaults.game'))->firstOrFail();

        return app(ShowStatistic::class)($request, $location, $game);
    }
}
