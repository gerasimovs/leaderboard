<?php

namespace App\Http\Controllers;

use App\Game;
use App\Session;
use Illuminate\Http\Request;

class ShowSession extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request, Game $game, Session $session)
    {
        if (!$game->is($session->game)) {
            abort(404);
        }

        $session->load(['players' => function ($query) {
            $query->with('team')
                ->orderBy('statistic.score', 'desc')
                ->orderBy('statistic.kills', 'asc');
        }]);

        $teams = $session->players->groupBy('team.title');
        $location = $session->location;

        return view('ratings.show', compact('session', 'teams', 'game', 'location'));
    }
}
