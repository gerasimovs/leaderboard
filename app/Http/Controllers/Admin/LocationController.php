<?php

namespace App\Http\Controllers\Admin;

use App\Location;
use Illuminate\Http\Request;

class LocationController extends Controller
{
    /**
     * LocationController constructor.
     */
    public function __construct()
    {
        $this->authorizeResource(Location::class, 'location');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('index', Location::class);

        $locations = Location::query()->paginate();

        return view('admin.locations.index', compact('locations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.locations.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'title' => 'required|min:2|unique:locations,title|max:191',
            'slug' => 'nullable|regex:/^[a-zA-Z0-9_]+$/u|max:191',
            'url' => 'nullable|url',
            'timezone' => 'required|timezone',
            'is_active' => 'boolean',
        ]);

        if (!isset($data['slug'])) {
            $i = 1;
            $originalSlug = \Illuminate\Support\Str::slug($data['title']);
            $slug = $originalSlug;

            while (Location::query()->where('slug', $slug)->exists()) {
                $slug = "{$slug}-{$i}";
                $i++;
            }

            $data['slug'] = $slug;
        }

        $location = new Location;
        $location->forceFill($data)->save();

        return back()->with('status', 'Новая локация создана');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function show(Location $location)
    {
        return view('admin.locations.show', compact('location'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function edit(Location $location)
    {
        return view('admin.locations.edit', compact('location'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Location $location)
    {
        $data = $request->validate([
            'title' => 'required|min:2|unique:locations,title,' . $location->id . '|max:255',
            'slug' => 'nullable|regex:/^[a-zA-Z0-9_]+$/u|max:255',
            'url' => 'nullable|url',
            'timezone' => 'required|timezone',
            'is_active' => 'boolean',
        ]);

        $location->forceFill($data)->save();

        return back()->with('status', 'Изменения локации сохранены');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function destroy(Location $location)
    {
        $location->delete();

        return redirect()->route('admin.locations.index')
            ->with('status', 'Локация удалена');
    }
}
