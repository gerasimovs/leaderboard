<?php

namespace App\Http\Controllers\Admin;

use App\Team;
use Illuminate\Http\Request;

class TeamController extends Controller
{
    /**
     * TeamController constructor.
     */
    public function __construct()
    {
        $this->authorizeResource(Team::class, 'team');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('index', Team::class);

        $teams = Team::query()
            ->with('players')
            ->latest('created_at')
            ->latest('id');

        $actionAll = $request->user()->actions->firstWhere('slug', 'teams.all');
        $canAll = $actionAll && json_decode($actionAll->permission->value) === true;

        if (!$canAll) {
            $actionLocation = $request->user()->actions->firstWhere('slug', 'teams.has_location');
            if ($actionLocation) {
                $locationSlug = json_decode($actionLocation->permission->value);
                $teams->whereHas('location', function ($query) use ($locationSlug) {
                    $query->where('slug', $locationSlug);
                });
            }
        }

        $teams = $teams->paginate();

        return view('admin.teams.index', compact('teams'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function show(Team $team)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function edit(Team $team)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Team $team)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function destroy(Team $team)
    {
        //
    }
}
