<?php

namespace App\Http\Controllers\Admin;

use App\Action;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class ActionController extends Controller
{
    /**
     * ActionController constructor.
     */
    public function __construct()
    {
        $this->authorizeResource(Action::class, 'action');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('index', Action::class);

        $actions = Action::query()->paginate();

        return view('admin.actions.index', compact('actions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.actions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'title' => 'required',
            'slug' => 'required|regex:/^[a-z\-\.]+$/u|unique:actions|max:191',
        ]);

        $action = new Action;
        $action->forceFill($data)->save();

        return back()->with('status', 'Новое разрешение создано');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Action  $action
     * @return \Illuminate\Http\Response
     */
    public function show(Action $action)
    {
        return view('admin.actions.show', compact('action'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Action  $action
     * @return \Illuminate\Http\Response
     */
    public function edit(Action $action)
    {
        return view('admin.actions.edit', compact('action'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Action  $action
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Action $action)
    {
        $data = $request->validate([
            'title' => 'required',
            'slug' => ['required', 'regex:/^[a-z\-\.]+$/u', Rule::unique('actions')->ignore($action->id), 'max:191',]
        ]);

        $action->forceFill($data)->save();

        return back()->with('status', 'Изменения сохранены');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Action  $action
     * @return \Illuminate\Http\Response
     */
    public function destroy(Action $action)
    {
        $action->delete();

        return redirect()->route('admin.actions.index')
            ->with('status', 'Резрешние удалено');
    }
}
