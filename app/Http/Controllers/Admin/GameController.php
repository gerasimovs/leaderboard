<?php

namespace App\Http\Controllers\Admin;

use App\Game;
use Illuminate\Http\Request;

class GameController extends Controller
{
    /**
     * GameController constructor.
     */
    public function __construct()
    {
        $this->authorizeResource(Game::class, 'game');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('index', Game::class);

        $games = Game::query()->paginate();

        return view('admin.games.index', compact('games'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.games.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'title' => 'required|min:2|unique:games,title|max:255',
            'slug' => 'nullable|regex:/^[a-zA-Z0-9_]+$/u|max:255',
            'is_active' => 'boolean',
        ]);

        if (!isset($data['slug'])) {
            $i = 1;
            $originalSlug = \Illuminate\Support\Str::slug($data['title']);
            $slug = $originalSlug;

            while (Game::query()->where('slug', $slug)->exists()) {
                $slug = "{$slug}-{$i}";
                $i++;
            }

            $data['slug'] = $slug;
        }

        $game = new Game;
        $game->forceFill($data)->save();

        return back()->with('status', 'Новая игра создана');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Game  $game
     * @return \Illuminate\Http\Response
     */
    public function show(Game $game)
    {
        return view('admin.games.show', compact('game'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Game  $game
     * @return \Illuminate\Http\Response
     */
    public function edit(Game $game)
    {
        return view('admin.games.edit', compact('game'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Game  $game
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Game $game)
    {
        $data = $request->validate([
            'title' => 'required|min:2|unique:games,title,' . $game->id . '|max:255',
            'slug' => 'nullable|regex:/^[a-zA-Z0-9_]+$/u|max:255',
            'is_active' => 'boolean',
        ]);

        $game->forceFill($data)->save();

        return back()->with('status', 'Изменения игры сохранены');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Game  $game
     * @return \Illuminate\Http\Response
     */
    public function destroy(Game $game)
    {
        $game->delete();

        return redirect()->route('admin.games.index')
            ->with('status', 'Игра удалена');
    }
}
