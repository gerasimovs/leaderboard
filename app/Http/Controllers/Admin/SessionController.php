<?php

namespace App\Http\Controllers\Admin;

use App\Session;
use Illuminate\Http\Request;

class SessionController extends Controller
{
    /**
     * SessionController constructor.
     */
    public function __construct()
    {
        $this->authorizeResource(Session::class, 'session');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('index', Session::class);

        $sessions = Session::query()
            ->with('players.team', 'files', 'game')
            ->latest('created_at')
            ->latest('id');

        $actionAll = $request->user()->actions->firstWhere('slug', 'sessions.all');
        $canAll = $actionAll && json_decode($actionAll->permission->value) === true;

        if (!$canAll) {
            $actionLocation = $request->user()->actions->firstWhere('slug', 'sessions.has_location');
            if ($actionLocation) {
                $locationSlug = json_decode($actionLocation->permission->value);
                $sessions->whereHas('location', function ($query) use ($locationSlug) {
                    $query->where('slug', $locationSlug);
                });
            }
        }

        $sessions = $sessions
            ->paginate();

        return view('admin.sessions.index', compact('sessions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Session  $session
     * @return \Illuminate\Http\Response
     */
    public function show(Session $session)
    {
        $session->load('files', 'players', 'players.team');
        return view('admin.sessions.show', compact('session'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Session  $session
     * @return \Illuminate\Http\Response
     */
    public function edit(Session $session)
    {
        $session->load('game');
        return view('admin.sessions.edit', compact('session'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Session  $session
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Session $session)
    {
        $files = [];

        if ($video = filter_var($request->get('video'), FILTER_VALIDATE_URL, FILTER_FLAG_PATH_REQUIRED)) {
            $files[] = [
                'path' => $video,
                'type' => 'video',
            ];
        }

        if ($photo = $request->file('photo')) {
            $files[] = [
                'path' => $photo->store('public/sessions'),
                'title' => $photo->getClientOriginalName(),
                'type' => 'photo',
            ];
        }

        if ($files) {
            $session->files()->createMany($files);
            return back()->with('status', 'Добавлены файлы в сессию');
        }

        return back()->with('status', 'Файлы отсутствуют');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Session  $session
     * @return \Illuminate\Http\Response
     */
    public function destroy(Session $session)
    {
        $session->delete();

        return redirect()->route('admin.sessions.index')
            ->with('status', 'Сессия удалена');
    }
}
