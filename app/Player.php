<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Player extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * Get the team that owns the player.
     */
    public function team()
    {
        return $this->belongsTo(Team::class);
    }

    /**
     * Get the statistic for the player.
     */
    public function statistics()
    {
        return $this->hasMany(Statistic::class);
    }

    /**
     * Get the statistic for the player.
     */
    public function session()
    {
        return $this->belongsTo(Session::class);
    }

    /**
     * Get the sessions for the player.
     */
    public function sessions()
    {
        return $this->belongsToMany(Session::class, 'statistic')
            ->using(Statistic::class)
            ->withPivot([
                'kills', 'deaths', 'team_kills', 'headshots', 'score',
            ])
            ->as('statistics');
    }
}
