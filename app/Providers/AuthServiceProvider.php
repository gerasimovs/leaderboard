<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        \App\Action::class => \App\Policies\ActionPolicy::class,
        \App\User::class => \App\Policies\UserPolicy::class,
        \App\Location::class => \App\Policies\LocationPolicy::class,
        \App\Game::class => \App\Policies\GamePolicy::class,
        \App\Team::class => \App\Policies\TeamPolicy::class,
        \App\Session::class => \App\Policies\SessionPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::before(function ($user, $ability) {
            $action = $user->actions->firstWhere('slug', 'dashboard.all');
            if ($action && json_decode($action->permission->value) === true) {
                return true;
            }
        });
    }
}
