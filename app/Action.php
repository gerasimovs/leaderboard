<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Action extends Model
{
    /**
     * Get the users for the action.
     */
    public function users()
    {
        return $this->belongsToMany(User::class)
            ->using(ActionUser::class)
            ->as('permissions');
    }
}
