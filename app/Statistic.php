<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;

class Statistic extends Pivot
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'kills', 'deaths', 'team_kills', 'headshots', 'score',
        'player_id', 'session_id'
    ];

    /**
     * Get the players for the statistic.
     */
    public function player()
    {
        return $this->belongsTo(Player::class);
    }

    /**
     * Get the session for the statistic.
     */
    public function session()
    {
        return $this->belongsTo(Session::class);
    }
}
