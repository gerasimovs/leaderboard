<?php

namespace App\Console\Commands;

use App\Statistic;
use App\Services\StatisticService;
use Illuminate\Console\Command;

class RecountScores extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'recount:scores';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Recount scores for statistics';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if ($this->confirm('Are you sure?')) {
            $service = app(StatisticService::class);
            $progressBar = $this->output->createProgressBar(Statistic::query()->count());

            Statistic::query()->each(function ($statistic) use ($service, $progressBar) {
                $statistic->score = $service->scoreCalculate($statistic->getAttributes());
                $statistic->save();
                $progressBar->advance();
            });

            $progressBar->finish();

            $this->output->newLine();
            $this->info('Recount scores is compleate!');
        }
    }
}
