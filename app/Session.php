<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Session extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'played_at', 'time', 'location_id', 'game_id'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        //'played_at' => 'datetime:Y-m-d',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'played_at',
    ];


    /**
     * Get the players for the session.
     */
    public function players()
    {
        return $this->belongsToMany(Player::class, 'statistic')
            ->using(Statistic::class)
            ->withPivot([
                'kills', 'deaths', 'team_kills', 'headshots', 'score',
            ])
            ->as('statistics');

    }

    /**
     * Get the game that owns the session.
     */
    public function game()
    {
        return $this->belongsTo(Game::class);
    }

    /**
     * Get the teams that owns the session.
     */
    public function team()
    {
        return $this->belongsTo(Team::class);
    }

    /**
     * Get the location that owns the session.
     */
    public function location()
    {
        return $this->belongsTo(Location::class);
    }

    /**
     * Get the files for the session.
     */
    public function files()
    {
        return $this->hasMany(File::class);
    }

    /**
     * Order by statistic and add to select totals
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOrderByTotals($query)
    {
        return $query->select('sessions.*', 'teams.id as team_id')
            ->selectRaw('SUM(statistic.kills)       as total_kills')
            ->selectRaw('SUM(statistic.headshots)   as total_headshots')
            ->selectRaw('SUM(statistic.team_kills)  as total_team_kills')
            ->selectRaw('SUM(statistic.deaths)      as total_deaths')
            ->selectRaw('SUM(statistic.score)       as total_score')
            ->join('statistic', 'sessions.id',      '=', 'statistic.session_id')
            ->join('players',   'players.id',       '=', 'statistic.player_id')
            ->join('teams',     'teams.id',         '=', 'players.team_id')
            ->groupBy('sessions.id', 'teams.id')
            ->orderByRaw('SUM(statistic.score) DESC');
    }
}
