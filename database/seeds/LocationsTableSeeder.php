<?php

use Illuminate\Database\Seeder;

class LocationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('locations')->insert([
            [
                'slug' => 'novosibirsk',
                'title' => 'Новосибирск',
                'timezone' => 'Asia/Bangkok',
                'is_active' => true,
            ],
            [
                'slug' => 'saratov',
                'title' => 'Саратов',
                'timezone' => 'Asia/Tbilisi',
                'is_active' => true,
            ],
            [
                'slug' => 'kazan',
                'title' => 'Казань',
                'timezone' => 'Europe/Moscow',
                'is_active' => false,
            ],
        ]);
    }
}
