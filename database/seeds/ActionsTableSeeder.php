<?php

use Illuminate\Database\Seeder;

class ActionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('actions')->insert(
            $this->getActions()
        );
    }

    public function getActions()
    {
        return [
            /*[
                'slug' => 'dashboard.show',
                'title' => 'Доступ в админ-панель',
                'type' => 'boolean',
            ],*/
            [
                'slug' => 'dashboard.all',
                'title' => 'Все разделы (полный доступ)',
                'type' => 'boolean',
            ],
            [
                'slug' => 'users.all',
                'title' => 'Пользователи (полный доступ)',
                'type' => 'boolean',
            ],
            [
                'slug' => 'locations.all',
                'title' => 'Локации (полный доступ)',
                'type' => 'boolean',
            ],
            [
                'slug' => 'games.all',
                'title' => 'Игры (полный доступ)',
                'type' => 'boolean',
            ],
            [
                'slug' => 'teams.all',
                'title' => 'Команды (полный доступ)',
                'type' => 'boolean',
            ],
            [
                'slug' => 'teams.has_location',
                'title' => 'Команды (только в локации)',
                'type' => 'relation:location',
            ],
            /*[
                'slug' => 'teams.has_game',
                'title' => 'Команды (только по игре)',
                'type' => 'relation:game',
            ],*/
            [
                'slug' => 'sessions.all',
                'title' => 'Сессии (полный доступ)',
                'type' => 'boolean',
            ],
            [
                'slug' => 'sessions.has_location',
                'title' => 'Сессии (только в локации)',
                'type' => 'relation:location',
            ],
            /*[
                'slug' => 'sessions.has_game',
                'title' => 'Сессии (только по игре)',
                'type' => 'relation:game',
            ],*/
        ];
    }
}
