<?php

use Illuminate\Database\Seeder;

class FakerStatisticsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $teams = factory(App\Team::class, 50)->create()->each([$this, 'afterCreateTeam']);

        $teams->each(function ($team) {
            $session = factory(App\Session::class)->create();
            $team->players->each(function ($player) use ($session) {
                $this->addSessionWithStatistic($player, $session);
            });
        });
    }

    public function afterCreateTeam($team)
    {
        $team->players()->saveMany(
            factory(App\Player::class, rand(2, 5))->make()
        );
    }

    public function addSessionWithStatistic($player, $session)
    {
        $player->sessions()->sync([
            $session->id => factory(App\Statistic::class)->make()->getAttributes()
        ], false);
    }
}
