<?php

use Illuminate\Database\Seeder;

class GamesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('games')->insert([
            [
                'slug' => 'pvp',
                'title' => 'PVP',
                'is_active' => true,
            ],
            [
                'slug' => 'safe-night',
                'title' => 'Safe Night',
                'is_active' => true,
            ],
        ]);
    }
}
