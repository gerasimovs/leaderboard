<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userId = DB::table('users')->insertGetId([
            'name' => 'root',
            'email' => 'root@thedeepvr.ru',
            'password' => bcrypt('root'),
            'is_active' => true,
        ]);

        if ($action = DB::table('actions')->where('slug', 'dashboard.all')->first()) {
            DB::table('action_user')->insert([
                'action_id' => $action->id,
                'user_id' => $userId,
                'value' => json_encode(true),
            ]);
        }
    }
}
