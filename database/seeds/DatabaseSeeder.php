<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    protected $seeders = [
        ActionsTableSeeder::class,
        UsersTableSeeder::class,
        LocationsTableSeeder::class,
        GamesTableSeeder::class,
    ];

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        echo PHP_EOL . "\033[0;32m" . 'Add faker data?' . "\033[0m" . ' [' . "\033[0;33m" . 'no' . "\033[0m" . '] ';
        $line = strtolower(stream_get_line(STDIN, 1024, PHP_EOL));
        if ($line === 'y' || $line === 'yes') {
            $this->seeders[] = FakerStatisticsSeeder::class;
        }

        $this->call($this->seeders);
    }
}
