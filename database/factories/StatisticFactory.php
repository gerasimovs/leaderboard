<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Statistic;
use App\Services\StatisticService;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$service = app(StatisticService::class);
$factory->define(Statistic::class, function (Faker $faker) use ($service) {
    $kills = $faker->numberBetween(3, 15);

    $data = [
        'kills' => $kills,
        'deaths' => $faker->numberBetween(1, round($kills * 8 / 10)),
        'team_kills' => $faker->numberBetween(0, $kills),
        'headshots' => $faker->numberBetween(0, $kills * 10),
    ];

    $data['score'] = $service->scoreCalculate($data);

    return $data;
});
