<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Game;
use App\Location;
use App\Session;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$locationIds = Location::query()->pluck('id')->toArray();
$gameIds = Game::query()->pluck('id')->toArray();

$factory->define(Session::class, function (Faker $faker) use ($locationIds, $gameIds) {
    return [
        'played_at' => $faker->date('Y-m-d', 'now'),
        'time' => $faker->numberBetween(25 * 60, 40 * 60),
        'location_id' => $faker->randomElement($locationIds),
        'game_id' => $faker->randomElement($gameIds),
    ];
});
