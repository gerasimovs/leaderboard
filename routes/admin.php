<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ShowDashboard')->name('index');

Route::resources([
    'locations' => 'LocationController',
    'teams' => 'TeamController',
    'games' => 'GameController',
    'sessions' => 'SessionController',
    'statistics' => 'StatisticController',
    'files' => 'FileController',
    'users' => 'UserController',
    'actions' => 'ActionController',
]);
