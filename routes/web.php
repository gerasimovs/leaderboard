<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes([
    'register' => false,
]);

Route::get('/home', 'HomeController@index');

Route::get('/', 'DinamicRoute')->name('home');
Route::get('players/stat', 'ShowPlayersStatistic')->name('players.ratings.world');
Route::get('players/{location}/stat', 'ShowPlayersStatistic')->name('players.ratings.location');
Route::get('{location}/stat', 'ShowStatistic')->name('locations.ratings.show');
Route::get('{game}/session/{session}', 'ShowSession')->name('sessions.show');
Route::get('{path}/stat', 'DynamicRoute')->where('path', '.*');
