<?php

use App\Action;
use App\Game;
use App\Location;
use App\Session;
use App\Team;
use App\User;

/* Dashboard */

Breadcrumbs::for('admin.index', function ($trail) {
    $trail->push(__('Dashboard'), route('admin.index'));
});

/* Locations */

Breadcrumbs::for('admin.users.index', function ($trail) {
    $trail->parent('admin.index');
    $trail->push(__('Users'), route('admin.users.index'));
});

Breadcrumbs::for('admin.users.create', function ($trail) {
    $trail->parent('admin.users.index');
    $trail->push(__('Create'), route('admin.users.create'));
});

Breadcrumbs::for('admin.users.show', function ($trail, $user) {
    $user = ($user instanceof User) ? $user : User::query()->findOrFail($user);
    $trail->parent('admin.users.index');
    $trail->push($user->name, route('admin.users.show', $user));
});

Breadcrumbs::for('admin.users.edit', function ($trail, $user) {
    $user = ($user instanceof User) ? $user : User::query()->findOrFail($user);
    $trail->parent('admin.users.show', $user);
    $trail->push(__('Edit'), route('admin.users.create', $user));
});

/* Locations */

Breadcrumbs::for('admin.locations.index', function ($trail) {
    $trail->parent('admin.index');
    $trail->push(__('Locations'), route('admin.locations.index'));
});

Breadcrumbs::for('admin.locations.create', function ($trail) {
    $trail->parent('admin.locations.index');
    $trail->push(__('Create'), route('admin.locations.create'));
});

Breadcrumbs::for('admin.locations.show', function ($trail, $location) {
    $location = ($location instanceof Location) ? $location : Location::query()->findOrFail($location);
    $trail->parent('admin.locations.index');
    $trail->push($location->title, route('admin.locations.show', $location));
});

Breadcrumbs::for('admin.locations.edit', function ($trail, $location) {
    $location = ($location instanceof Location) ? $location : Location::query()->findOrFail($location);
    $trail->parent('admin.locations.show', $location);
    $trail->push(__('Edit'), route('admin.locations.create', $location));
});

/* Games */

Breadcrumbs::for('admin.games.index', function ($trail) {
    $trail->parent('admin.index');
    $trail->push(__('Games'), route('admin.games.index'));
});

Breadcrumbs::for('admin.games.create', function ($trail) {
    $trail->parent('admin.games.index');
    $trail->push(__('Create'), route('admin.games.create'));
});

Breadcrumbs::for('admin.games.show', function ($trail, $game) {
    $game = ($game instanceof Game) ? $game : Game::query()->findOrFail($game);
    $trail->parent('admin.games.index');
    $trail->push($game->title, route('admin.games.show', $game));
});

Breadcrumbs::for('admin.games.edit', function ($trail, $game) {
    $game = ($game instanceof Game) ? $game : Game::query()->findOrFail($game);
    $trail->parent('admin.games.show', $game);
    $trail->push(__('Edit'), route('admin.games.create', $game));
});

/* Teams */

Breadcrumbs::for('admin.teams.index', function ($trail) {
    $trail->parent('admin.index');
    $trail->push(__('Teams'), route('admin.teams.index'));
});

Breadcrumbs::for('admin.teams.create', function ($trail) {
    $trail->parent('admin.teams.index');
    $trail->push(__('Create'), route('admin.teams.create'));
});

Breadcrumbs::for('admin.teams.show', function ($trail, $team) {
    $team = ($team instanceof Team) ? $team : Team::query()->findOrFail($team);
    $trail->parent('admin.teams.index');
    $trail->push($team->title, route('admin.teams.show', $team));
});

Breadcrumbs::for('admin.teams.edit', function ($trail, $team) {
    $team = ($team instanceof Team) ? $team : Team::query()->findOrFail($team);
    $trail->parent('admin.teams.show', $team);
    $trail->push(__('Edit'), route('admin.teams.create', $team));
});

/* Sessions */

Breadcrumbs::for('admin.sessions.index', function ($trail) {
    $trail->parent('admin.index');
    $trail->push(__('Sessions'), route('admin.sessions.index'));
});

Breadcrumbs::for('admin.sessions.create', function ($trail) {
    $trail->parent('admin.sessions.index');
    $trail->push(__('Create'), route('admin.sessions.create'));
});

Breadcrumbs::for('admin.sessions.show', function ($trail, $session) {
    $session = ($session instanceof Session) ? $session : Session::query()->findOrFail($session);
    $trail->parent('admin.sessions.index');
    $trail->push(
        app(\App\Services\TimezoneService::class)->getForSession($session->played_at, $session),
        route('admin.sessions.show', $session)
    );
});

Breadcrumbs::for('admin.sessions.edit', function ($trail, $session) {
    $session = ($session instanceof Session) ? $session : Session::query()->findOrFail($session);
    $trail->parent('admin.sessions.show', $session);
    $trail->push(__('Edit'), route('admin.sessions.create', $session));
});

/* Actions */

Breadcrumbs::for('admin.actions.index', function ($trail) {
    $trail->parent('admin.index');
    $trail->push(__('Actions'), route('admin.actions.index'));
});

Breadcrumbs::for('admin.actions.create', function ($trail) {
    $trail->parent('admin.actions.index');
    $trail->push(__('Create'), route('admin.actions.create'));
});

Breadcrumbs::for('admin.actions.show', function ($trail, $action) {
    $action = ($action instanceof Action) ? $action : Action::query()->findOrFail($action);
    $trail->parent('admin.actions.index');
    $trail->push($action->title, route('admin.actions.show', $action));
});

Breadcrumbs::for('admin.actions.edit', function ($trail, $action) {
    $action = ($action instanceof Action) ? $action : Action::query()->findOrFail($action);
    $trail->parent('admin.actions.show', $action);
    $trail->push(__('Edit'), route('admin.actions.create', $action));
});

