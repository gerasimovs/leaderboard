<?php

return [
    'defaults' => [
        'game' => 'safe-night',
    ],

    'social' => [
        'novosibirsk' => [
            'facebook' => 'https://www.facebook.com/nsk.thedeepvr/',
            'vk' => 'https://vk.com/deepnsk',
            'youtube' => 'https://www.youtube.com/channel/UCHDatoDOYR_tFFZt1RRWd6w',
            'instagram' => 'https://www.instagram.com/thedeepvr/',
        ],
    ],

    'languages' => [
        [
            'title' => 'Русский',
            'abbr' => 'ru',
            'flag' => 'img/flags/ru.svg',
            'is_show' => true,
        ],
        [
            'title' => 'English',
            'abbr' => 'en',
            'flag' => 'img/flags/en.svg',
            'is_show' => true,
        ],
    ],

    'menu' => [
        'users' => [
            'is_show' => true,
            'permission' => ['index', App\User::class],
            'route' => [
                'admin.users.index',
            ],
            'aliases' => [
                'admin.users.show',
                'admin.users.edit',
            ],
            'title' => 'Users',
            'children' => [
                [
                    'is_show' => true,
                    'route' => [
                        'admin.users.create',
                    ],
                    'title' => 'Create',
                ],
            ],
        ],
        'locations' => [
            'is_show' => true,
            'permission' => ['index', App\Location::class],
            'route' => [
                'admin.locations.index',
            ],
            'aliases' => [
                'admin.locations.show',
                'admin.locations.edit',
            ],
            'title' => 'Locations',
            'children' => [
                [
                    'is_show' => true,
                    'route' => [
                        'admin.locations.create',
                    ],
                    'title' => 'Create',
                ],
            ],
        ],
        'games' => [
            'is_show' => true,
            'permission' => ['index', App\Location::class],
            'route' => [
                'admin.games.index',
            ],
            'aliases' => [
                'admin.games.show',
                'admin.games.edit',
            ],
            'title' => 'Games',
            'children' => [
                [
                    'is_show' => true,
                    'route' => [
                        'admin.games.create',
                    ],
                    'title' => 'Create',
                ],
            ],
        ],
        'teams' => [
            'is_show' => true,
            'permission' => ['index', App\Team::class],
            'route' => [
                'admin.teams.index',
            ],
            'title' => 'Teams',
        ],
        'sessions' => [
            'is_show' => true,
            'permission' => ['index', App\Session::class],
            'route' => [
                'admin.sessions.index',
            ],
            'title' => 'Sessions',
        ],
        'statistics' => [
            'is_show' => false,
            'permission' => ['index', App\Statistic::class],
            'route' => [
                'admin.statistics.index',
            ],
            'title' => 'Statistic',
        ],
        'files' => [
            'is_show' => false,
            'permission' => ['index', App\File::class],
            'route' => [
                'admin.files.index',
            ],
            'title' => 'Files',
        ],
    ],

];
