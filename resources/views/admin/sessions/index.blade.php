@extends('admin.layouts.app')

@section('content')
    @if (isset($sessions) && $sessions->total() > 0)
        @inject('timezone', 'App\Services\TimezoneService')
        <table class="table is-narrow is-hoverable is-fullwidth">
            <thead>
                <tr>
                    <th>@lang('Date')</th>
                    {{-- <th>@lang('Time')</th> --}}
                    <th>@lang('Team')</th>
                    <th>@lang('Files')</th>
                    <th>@lang('Actions')</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($sessions as $session)
                    @php
                        // $minutes = sprintf("%02d", floor($session->time / 60));
                        // $seconds = sprintf("%02d", $session->time - $minutes * 60);
                        $timezoneId = $session->location->timezone ?? null;
                    @endphp
                    <tr>
                        <td>
                            {{ $session->played_at->setTimezone($timezoneId ?? 'UTC')->format('m.d.Y H:i') }}
                            @isset($timezoneId)
                                <div class="is-size-7">tz: {{ $timezone->getDisplay($timezoneId) }}</div>
                            @endisset
                        </td>
                        {{-- <td>{{ $minutes }}:{{ $seconds }}</td> --}}
                        <td>
                            {{ $session->players->pluck('team.title')->unique()->implode(' : ') ?? 'Не выбрана' }}
                            @isset($session->location)
                                <div class="is-size-7">{{ $session->location->title }}</div>
                            @endisset
                        </td>
                        <td>
                            <span class="@if($session->files->where('type', 'photo')->isNotEmpty()) has-text-success @else has-text-danger @endif">
                                <i class="far fa-image"></i>
                            </span>
                            <span class="@if($session->files->where('type', 'video')->isNotEmpty()) has-text-success @else has-text-danger @endif">
                                <i class="fas fa-film"></i>
                            </span>
                        </td>
                        <td>
                            @can('update', $session)
                                <div>
                                    <a href="{{ route('admin.sessions.edit', $session) }}">@lang('Edit')</a>
                                </div>
                            @endcan
                            @can('delete', $session)
                                <div>
                                    <a href="{{ route('admin.sessions.destroy', $session) }}" @click.prevent="deleteEntity($event)">@lang('Delete')</a>
                                </div>
                            @endcan
                            @can('view', $session)
                                <div>
                                    <a href="{{ route('sessions.show', ['game' => $session->game, 'session' => $session]) }}" @click.prevent="copyToClipboard('{{ route('sessions.show', ['game' => $session->game, 'session' => $session]) }}')">@lang('Copy')</a>
                                </div>
                            @endcan
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        @include('admin.partials.pagination', [
            'collection' => $sessions,
        ])
    @else
        <article class="message is-link">
            <div class="message-body">
                @lang('No sessions')
            </div>
        </article>
    @endif
@endsection
