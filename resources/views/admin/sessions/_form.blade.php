<fieldset class="field" disabled>
    @inject('timezone', 'App\Services\TimezoneService')
    @component('admin.components.input', [
        'label' => __('Date'),
        'value' => isset($session->played_at)
            ? $timezone->getForSession($session->played_at, $session)
            : null,
        'name' => 'played_at'
    ])@endcomponent

    @php
        $sessionTime = $session->time ?? 0;
        $minutes = floor($sessionTime / 60);
        $seconds = $sessionTime - $minutes * 60;
    @endphp

    @component('admin.components.input', [
        'label' => __('Time'),
        'value' => "{$minutes}:{$seconds}",
        'name' => 'time',
    ])@endcomponent
</fieldset>

@component('admin.components.file', [
    'label' => __('Photo'),
    'labelButton' => __('Выберите фотографию'),
    'value' => null,
    'name' => 'photo',
])@endcomponent

@if (isset($session) && $session->files->where('type', 'photo')->isNotEmpty())
<ul class="tags">
    @foreach ($session->files->where('type', 'photo') as $file)
        <li class="tag">{{ asset(Storage::url($file->path)) }} <a href="{{ route('admin.files.destroy', $file) }}" style="margin-left: .5rem;" @click.prevent="deleteEntity($event)"><i class="fas fa-trash"></i></a></li>
    @endforeach
</ul>
@endif

@component('admin.components.input', [
    'label' => __('Video'),
    'value' => null,
    'name' => 'video',
])@endcomponent

@if (isset($session) && $session->files->where('type', 'video')->isNotEmpty())
<ul class="tags">
    @foreach ($session->files->where('type', 'video') as $file)
        <li class="tag">{{ asset($file->path) }} <a href="{{ route('admin.files.destroy', $file) }}" style="margin-left: .5rem;" @click.prevent="deleteEntity($event)"><i class="fas fa-trash"></i></a></li>
    @endforeach
</ul>
@endif

<div class="field is-grouped">
    @component('admin.components.submit', [
        'label' => isset($session) ? __('Save') : __('Create'),
    ])@endcomponent
    @isset($session)
        <div class="control">
            <a class="button is-text" href="{{ route('sessions.show', ['game' => $session->game, 'session' => $session]) }}" @click.prevent="copyToClipboard('{{ route('sessions.show', ['game' => $session->game, 'session' => $session]) }}')">@lang('Copy')</a>
        </div>
    @endisset
</div>
