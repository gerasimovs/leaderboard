@extends('admin.layouts.app')

@section('content')
    <form action="{{ route('admin.sessions.update', $session) }}" enctype="multipart/form-data" method="post">
        @csrf
        @method('put')
        @include('admin.sessions._form')
    </form>
@endsection
