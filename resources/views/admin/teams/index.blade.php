@extends('admin.layouts.app')

@section('content')
    @if (isset($teams) && $teams->total() > 0)
        <table class="table is-narrow is-hoverable is-fullwidth">
            <thead>
                <tr>
                    <th>@lang('Title')</th>
                    <th>@lang('Players')</th>
                    {{-- <th>@lang('Actions')</th> --}}
                </tr>
            </thead>
            <tbody>
                @foreach ($teams as $team)
                    <tr>
                        <td>{{ $team->title }}</td>
                        <td><abbr title="{{ $team->players->implode('name', '; ') }}">{{ $team->players->count() }}</abbr></td>
                        {{-- <td>
                            <a href="{{ route('admin.teams.edit', $team) }}">@lang('Edit')</a>
                            <form action="{{ route('admin.teams.destroy', $team) }}" method="post">
                                @csrf
                                @method('delete')
                                <a onclick="this.closest('form').submit();">@lang('Delete')</a>
                            </form>
                        </td> --}}
                    </tr>
                @endforeach
            </tbody>
        </table>
        @include('admin.partials.pagination', [
            'collection' => $teams,
        ])
    @else
        <article class="message is-link">
            <div class="message-body">
                @lang('No teams')
            </div>
        </article>
    @endif
@endsection
