@php
	use Illuminate\Support\Str;
@endphp

@component('admin.components.input', [
    'label' => __('Title'),
    'name' => 'title',
    'value' => $action->title ?? null,
    'required' => true,
])@endcomponent

@component('admin.components.input', [
    'label' => __('Slug'),
    'value' => $action->slug ?? null,
    'name' => 'slug',
])@endcomponent

@component('admin.components.select', [
    'label' => __('Type'),
    'value' => isset($action)
        ? (Str::contains($action->type, ':') ? Str::before($action->type, ':') : $action->type)
        : null,
    'name' => 'type',
    'items' => [
    	null => '-- Выберите из списка --',
    	'boolean' => 'Да / Нет',
    	'relation' => [
            null => 'Связан с...',
            'relation:location' => 'Локация',
            'relation:game' => 'Игра',
        ],
    ],
])@endcomponent

@component('admin.components.submit', [
    'label' => isset($action) ? __('Save') : __('Create'),
])@endcomponent
