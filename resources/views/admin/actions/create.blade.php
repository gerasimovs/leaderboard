@extends('admin.layouts.app')

@section('content')
    <form action="{{ route('admin.actions.store') }}" method="post">
        @csrf
        @include('admin.actions._form')
    </form>
@endsection
