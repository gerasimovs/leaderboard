@extends('admin.layouts.app')

@section('content')
    @if (isset($actions) && $actions->total() > 0)
        <table class="table is-narrow is-hoverable is-fullwidth">
            <thead>
                <tr>
                    <th>@lang('Title')</th>
                    <th>@lang('Slug')</th>
                    <th>@lang('Type')</th>
                    <th>@lang('Actions')</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($actions as $action)
                    <tr>
                        <td>{{ $action->title }}</td>
                        <td>{{ $action->slug }}</td>
                        <td>{{ $action->type }}</td>
                        <td>
                            <div>
                                <a href="{{ route('admin.actions.edit', $action) }}">@lang('Edit')</a>
                            </div>
                            <div>
                                <a href="{{ route('admin.actions.destroy', $action) }}" @click.prevent="deleteEntity($event)">@lang('Delete')</a>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        @include('admin.partials.pagination', [
            'collection' => $actions,
        ])
    @else
        <article class="message is-link">
            <div class="message-body">
                @lang('No actions')
            </div>
        </article>
    @endif
@endsection
