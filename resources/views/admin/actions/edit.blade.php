@extends('admin.layouts.app')

@section('content')
    <form action="{{ route('admin.actions.update', $action) }}" method="post">
        @csrf
        @method('put')
        @include('admin.actions._form')
    </form>
@endsection
