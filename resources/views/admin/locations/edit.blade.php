@extends('admin.layouts.app')

@section('content')
    <form action="{{ route('admin.locations.update', $location) }}" method="post">
        @csrf
        @method('put')
        @include('admin.locations._form')
    </form>
@endsection
