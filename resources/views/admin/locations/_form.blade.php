@component('admin.components.input', [
    'label' => __('Title'),
    'name' => 'title',
    'value' => $location->title ?? null,
    'required' => true,
])@endcomponent

@component('admin.components.input', [
    'label' => __('Slug'),
    'value' => $location->slug ?? null,
    'name' => 'slug',
])@endcomponent

@component('admin.components.input', [
    'label' => __('Web-site'),
    'value' => $location->url ?? null,
    'name' => 'url',
])@endcomponent

@component('admin.components.checkbox', [
    'label' => __('Activity'),
    'value' => $location->is_active ?? true,
    'name' => 'is_active',
])@endcomponent

@inject('timezone', 'App\Services\TimezoneService')
@component('admin.components.select', [
    'label' => __('Timezone'),
    'value' => $location->timezone ?? null,
    'name' => 'timezone',
    'items' => $timezone->getList(),
])@endcomponent

@component('admin.components.submit', [
    'label' => isset($location) ? __('Save') : __('Create'),
])@endcomponent
