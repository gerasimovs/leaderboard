@extends('admin.layouts.app')

@section('content')
    @if (isset($locations) && $locations->total() > 0)
        @inject('timezone', 'App\Services\TimezoneService')
        <table class="table is-narrow is-hoverable is-fullwidth">
            <thead>
                <tr>
                    <th>@lang('Title')</th>
                    <th>@lang('Slug')</th>
                    <th>@lang('Activity')</th>
                    <th>@lang('Actions')</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($locations as $location)
                    <tr>
                        <td>
                            {{ $location->title }}
                            @if ($location->timezone)
                                <div class="is-size-7">tz: {{ $timezone->getDisplay($location->timezone) }}</div>
                            @endif
                        </td>
                        <td>{{ $location->slug }}</td>
                        <td>{{ $location->is_active ? 'да' : 'нет' }}</td>
                        <td>
                            <div>
                                <a href="{{ route('admin.locations.edit', $location) }}">@lang('Edit')</a>
                            </div>
                            <div>
                                <a href="{{ route('admin.locations.destroy', $location) }}" @click.prevent="deleteEntity($event)">@lang('Delete')</a>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        @include('admin.partials.pagination', [
            'collection' => $locations,
        ])
    @else
        <article class="message is-link">
            <div class="message-body">
                @lang('No locations')
            </div>
        </article>
    @endif
@endsection
