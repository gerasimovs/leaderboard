@extends('admin.layouts.app')

@section('content')
    <form action="{{ route('admin.locations.store') }}" method="post">
        @csrf
        @include('admin.locations._form')
    </form>
@endsection
