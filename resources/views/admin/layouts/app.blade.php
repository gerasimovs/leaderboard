<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>The Deep VR - LeaderBoard</title>
    <link defer rel="stylesheet" href="https://unpkg.com/buefy/dist/buefy.min.css">
    <script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>
  </head>
  <body>
    <!-- START NAV -->
    <nav class="navbar is-white">
        <div class="container">
            <div class="navbar-brand">
                <a class="navbar-item brand-text" href="{{ route('admin.index') }}">LeaderBoard Admin</a>
                <div class="navbar-burger burger" data-target="navMenu">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </div>
            <div class="navbar-menu">
                <div class="navbar-end">
                    @inject('languageService', 'App\Services\LanguageService')
                    <div class="navbar-item has-dropdown is-hoverable">
                        <span class="navbar-item">
                            <img src="{{ asset($languageService->getCurrent()['flag']) }}" style="height: 1rem; margin-right: .2rem;">
                            {{ $languageService->getCurrent()['title'] }}
                        </span>
                        <div class="navbar-dropdown is-boxed">
                            @foreach($languageService->getItems() as $language)
                                @continue($language['is_active'])
                                <a class="navbar-item" href="{{ request()->fullUrlWithQuery(['lang' => $language['abbr']]) }}">
                                    <img src="{{ asset($language['flag']) }}" style="height: 1rem; margin-right: .2rem;">
                                    {{ $language['title'] }}
                                </a>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </nav>
    <!-- END NAV -->
    <div class="container" id="app">
        <div class="columns">
            <div class="column is-3 ">
                @include('admin.partials.aside')
            </div>
            <div class="column is-9">
                @include('admin.partials.breadcrumb')
                @include('admin.partials.notifications')
                <div style="display: block; width: 100%; overflow-x: auto; -webkit-overflow-scrolling: touch; padding: .5rem;">
                    @yield('content')
                </div>
                @yield('pagination')
            </div>
        </div>
    </div>
    {{-- <script src="https://unpkg.com/vue"></script> --}}
    {{-- <script src="https://unpkg.com/buefy/dist/buefy.min.js"></script> --}}
    <script src="{{ mix('js/dashboard.js') }}"></script>
  </body>
</html>
