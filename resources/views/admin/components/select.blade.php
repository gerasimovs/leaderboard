@php
    /** @var Illuminate\Support\MessageBag $errors */
    $error = $errors->first($name);
    $label = $label ?? '';
    $value = old($name) ?? $value ?? null;
    $type = $type ?? 'text';
@endphp

<div class="field">
    <label class="label">{{ $label }}</label>
    <div class="control">
        <div class="select">
            <select
                name="{{ $name }}"
                class="input {{ $error ? 'is-danger' : '' }}"
                type="{{ $type }}"
                {{ isset($required) && $required === true ? 'required' : '' }}>
                @foreach($items as $itemValue => $itemLabel)
                    @component('admin.components.option', [
                        'selectedValue' => $value,
                        'value' => $itemValue,
                        'label' => $itemLabel
                    ]))@endcomponent
                @endforeach
            </select>
        </div>
    </div>
    @if ($error)
        <p class="help is-danger">{{ $error }}</p>
    @endif
</div>
