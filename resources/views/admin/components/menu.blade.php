<ul class="{{ $class ?? '' }}">
    @foreach ($list as $menuItem)
        @continue(isset($menuItem['is_show']) && $menuItem['is_show'] === false)
        @php
            $route = isset($menuItem['route']) ? route(...$menuItem['route']) : '#';
        @endphp
        <li>
            <a @if ($menuItem['active']) class="is-active" @endif href="{{ $route }}">
                {{ $menuItem['title'] }}
            </a>
            @if (isset($menuItem['children']) && ($menuItem['active'] || $menuItem['has_active']))
                @component('admin.components.menu', [
                    'list' => $menuItem['children'],
                ])@endcomponent
            @endif
        </li>
    @endforeach
</ul>
