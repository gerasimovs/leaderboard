@if ($paginator->total() > $paginator->perPage())
    <nav class="pagination is-small" role="navigation" aria-label="pagination">
        <a class="pagination-previous"
            href="{{ $paginator->previousPageUrl() }}"
            @if ($paginator->currentPage() <= 1) disabled @endif>Previous</a>
        <a class="pagination-next"
            href="{{ $paginator->nextPageUrl() }}"
            @if ($paginator->currentPage() >= $paginator->lastPage()) disabled @endif>Next page</a>

        <ul class="pagination-list">
            @foreach ($elements as $group)
                @if (is_string($group))
                    <li><span class="pagination-ellipsis">&hellip;</span></li>
                    @continue
                @endif

                @foreach ($group as $page => $url)
                    @php
                        $isCurrent = $paginator->currentPage() === $page;
                    @endphp
                    <li>
                        <a href="{{ $url }}"
                            class="pagination-link {{ $isCurrent ? 'is-current' : '' }}"
                            aria-label="Goto page {{ $page }}"
                            @if ($isCurrent) aria-current="page" @endif>
                            {{ $page }}
                        </a>
                    </li>
                @endforeach
            @endforeach
        </ul>

    </nav>
@endif
