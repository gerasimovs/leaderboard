<div class="control">
    <button class="button is-link">{{ $label ?? __('Submit') }}</button>
</div>
