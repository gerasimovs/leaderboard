@php
    /** @var Illuminate\Support\MessageBag $errors */
    $error = $errors->first($name);
@endphp

<div class="field">
    <label class="label">{{ $label ?? '' }}</label>
    <div class="control">
        <div class="file has-name">
          <label class="file-label">
            <input
                class="file-input {{ $error ? 'is-danger' : '' }}"
                type="file" name="{{ $name }}"
                value="{{ old($name) ?? $value ?? '' }}"
                {{ isset($required) && $required === true ? 'required' : '' }}
                onchange="this.closest('.file-label').querySelector('.file-name').innerText = this.files.length ? this.files[0].name : '';">
            <span class="file-cta">
              <span class="file-icon">
                <i class="fas fa-upload"></i>
              </span>
              <span class="file-label">
                {{ $labelButton ?? __('Choose a file') }}
              </span>
            </span>
            <span class="file-name">
            </span>
          </label>
        </div>
    </div>
    @if ($error)
        <p class="help is-danger">{{ $error }}</p>
    @endif
</div>
