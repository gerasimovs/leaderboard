@php
    /** @var Illuminate\Support\MessageBag $errors */
    $error = $errors->first($name);
@endphp

<div class="field">
    <label class="label">{{ $label ?? '' }}</label>
    <div class="control">
        <input
            name="{{ $name }}"
            class="input {{ $error ? 'is-danger' : '' }}"
            type="{{ $type ?? 'text' }}"
            @isset($placeholder) placeholder="{{ $placeholder }}" @endisset
            value="{{ old($name) ?? $value ?? '' }}"
            @isset($autocomplete) autocomplete="{{ $autocomplete }}" @endisset
            {{ isset($required) && $required === true ? 'required' : '' }}>
    </div>
    @if ($error)
        <p class="help is-danger">{{ $error }}</p>
    @endif
</div>
