@php
    /** @var Illuminate\Support\MessageBag $errors */
    $error = $errors->first($name);
@endphp

<div class="field">
    <div class="control">
        <label class="checkbox">
            <input type="hidden" name="{{ $name }}" value="0">
            <input type="checkbox" name="{{ $name }}" value="1" @if (isset($value) && $value === true) checked @endif>
            {{ $label ?? '' }}
        </label>
    </div>
</div>
