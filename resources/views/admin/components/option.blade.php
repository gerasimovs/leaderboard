@if (is_array($label))
    <optgroup label="{{ array_shift($label) }}">
        @foreach ($label as $itemValue => $itemLabel)
            @component('admin.components.option', [
                'selectedValue' => $selectedValue,
                'value' => $itemValue,
                'label' => $itemLabel
            ]))@endcomponent
        @endforeach
    </optgroup>
@else
    @php
        $selected = $selectedValue === $value;
    @endphp
    <option value="{{ $value }}" @if ($selected) selected @endif>
        {{ $label }}
    </option>
@endif
