@inject('menu', 'App\Services\Admin\AsideService')

<aside class="menu is-hidden-mobile" id="navMenu">
    <p class="menu-label">
        LeaderBoard
    </p>
    @component('admin.components.menu', [
        'list' => $menu
            ->setActive(Route::currentRouteName())
            ->getItems(),
        'class' => 'menu-list',
    ])@endcomponent
    <p class="menu-label">
        General
    </p>
    <form class="menu-list" action="{{ route('logout') }}" method="POST">
        @csrf
        <a href="{{ route('logout') }}" onclick="this.closest('form').submit(); return false;">
            @lang('Logout')
        </a>
    </form>
</aside>
