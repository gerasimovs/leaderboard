@if (session('status'))
    <div class="notification">
        {{ session('status') }}
    </div>
@endif
