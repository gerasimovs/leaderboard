@if (class_exists('Breadcrumbs') && Breadcrumbs::exists())
    {{ Breadcrumbs::view('breadcrumbs::bulma') }}
@endif
