@section('pagination')
    @if (isset($collection) && method_exists($collection, 'render'))
        {{ $collection->render('admin.components.pagination') }}
    @endif
@endsection
