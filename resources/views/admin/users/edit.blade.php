@extends('admin.layouts.app')

@section('content')
    <form action="{{ route('admin.users.update', $user) }}" method="post">
        @csrf
        @method('put')
        @include('admin.users._form')
    </form>
@endsection
