@extends('admin.layouts.app')

@section('content')
    @if (isset($users) && $users->total() > 0)
        <table class="table is-narrow is-hoverable is-fullwidth">
            <thead>
                <tr>
                    <th>@lang('Name')</th>
                    <th>@lang('E-mail')</th>
                    <th>@lang('Activity')</th>
                    <th>@lang('Actions')</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($users as $user)
                    <tr>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->is_active ? 'да' : 'нет' }}</td>
                        <td>
                            <div>
                                <a href="{{ route('admin.users.edit', $user) }}">@lang('Edit')</a>
                            </div>
                            <div>
                                <a href="{{ route('admin.users.destroy', $user) }}" @click.prevent="deleteEntity($event)">@lang('Delete')</a>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        @include('admin.partials.pagination', [
            'collection' => $users,
        ])
    @else
        <article class="message is-link">
            <div class="message-body">
                @lang('No users')
            </div>
        </article>
    @endif
@endsection
