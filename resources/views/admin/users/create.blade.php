@extends('admin.layouts.app')

@section('content')
    <form action="{{ route('admin.users.store') }}" method="post">
        @csrf
        @include('admin.users._form')
    </form>
@endsection
