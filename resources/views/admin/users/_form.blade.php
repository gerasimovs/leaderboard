<b-tabs style="margin-bottom: .75rem;">
    <b-tab-item label="General">
        @component('admin.components.input', [
            'label' => __('Name'),
            'name' => 'name',
            'value' => $user->name ?? null,
            'required' => true,
        ])@endcomponent

        @component('admin.components.input', [
            'label' => __('E-mail'),
            'value' => $user->email ?? null,
            'name' => 'email',
        ])@endcomponent

        @component('admin.components.input', [
            'label' => __('Password'),
            'type' => 'password',
            'value' => null,
            'name' => 'password',
            'autocomplete' => 'new-password',
        ])@endcomponent

        @component('admin.components.input', [
            'label' => __('Confirm Password'),
            'type' => 'password',
            'value' => null,
            'name' => 'password_confirmation',
        ])@endcomponent

        @component('admin.components.checkbox', [
            'label' => __('Activity'),
            'value' => $user->is_active ?? true,
            'name' => 'is_active',
        ])@endcomponent
    </b-tab-item>
    <b-tab-item label="Permissions">
        @inject('actions', 'App\Services\Admin\ActionService')
        @foreach($actions->getItems() as $action)
            @php
                $actionValue = isset($user)
                    ? ($user->actions->firstWhere('slug', $action->slug)->permission->value ?? null)
                    : null;
                $permissionName = 'permissions[' . $action->slug . ']';
            @endphp

            @if ($action->type === 'boolean')
                @component('admin.components.checkbox', [
                    'label' => $action->title,
                    'value' => $actionValue ?? false,
                    'name' => $permissionName,
                ])@endcomponent
            @elseif (Str::before($action->type, ':') === 'relation')
                <div
                    is="{{ Str::plural(Str::after($action->type, ':')) }}-list"
                    label="{{ $action->title }}"
                    name="{{ $permissionName }}"
                    value="{{ $actionValue }}"
                ></div>
            @endif
        @endforeach
    </b-tab-item>
</b-tabs>

@component('admin.components.submit', [
    'label' => isset($user) ? __('Save') : __('Create'),
])@endcomponent
