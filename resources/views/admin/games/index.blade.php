@extends('admin.layouts.app')

@section('content')
    @if (isset($games) && $games->total() > 0)
        <table class="table is-narrow is-hoverable is-fullwidth">
            <thead>
                <tr>
                    <th>@lang('Title')</th>
                    <th>@lang('Slug')</th>
                    <th>@lang('Activity')</th>
                    <th>@lang('Actions')</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($games as $game)
                    <tr>
                        <td>{{ $game->title }}</td>
                        <td>{{ $game->slug }}</td>
                        <td>{{ $game->is_active ? 'да' : 'нет' }}</td>
                        <td>
                            <div>
                                <a href="{{ route('admin.games.edit', $game) }}">@lang('Edit')</a>
                            </div>
                            <div>
                                <a href="{{ route('admin.games.destroy', $game) }}" @click.prevent="deleteEntity($event)">@lang('Delete')</a>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        @include('admin.partials.pagination', [
            'collection' => $games,
        ])
    @else
        <article class="message is-link">
            <div class="message-body">
                @lang('No games')
            </div>
        </article>
    @endif
@endsection
