@extends('admin.layouts.app')

@section('content')
    <form action="{{ route('admin.games.update', $game) }}" method="post">
        @csrf
        @method('put')
        @include('admin.games._form')
    </form>
@endsection
