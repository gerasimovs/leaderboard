@component('admin.components.input', [
    'label' => __('Title'),
    'name' => 'title',
    'value' => $game->title ?? null,
    'required' => true,
])@endcomponent

@component('admin.components.input', [
    'label' => __('Slug'),
    'value' => $game->slug ?? null,
    'name' => 'slug',
])@endcomponent

@component('admin.components.checkbox', [
    'label' => __('Activity'),
    'value' => $game->is_active ?? true,
    'name' => 'is_active',
])@endcomponent

@component('admin.components.submit', [
    'label' => isset($game) ? __('Save') : __('Create'),
])@endcomponent
