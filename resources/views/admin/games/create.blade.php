@extends('admin.layouts.app')

@section('content')
    <form action="{{ route('admin.games.store') }}" method="post">
        @csrf
        @include('admin.games._form')
    </form>
@endsection
