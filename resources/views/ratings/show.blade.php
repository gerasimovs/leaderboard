@extends('layouts.board')

@php
    // $minutes = sprintf("%02d", floor($session->time / 60));
    // $seconds = sprintf("%02d", $session->time - $minutes * 60);
    $image = $session->files->where('type', 'photo')->sortByDesc('id')->first();
    $video = $session->files->where('type', 'video')->sortByDesc('id')->first();
    $imagePath = $image ? asset(Storage::url($image->path)) : null;
    $videoPath = $video ? asset($video->path) : null;
@endphp

@section('og:title')Результаты моей игры@endsection

@section('og:image'){{ $imagePath ?? asset('img/no_photo.png') }}@endsection

@section('rating')


    @foreach ($teams as $team => $players)
        <h3 class="mb-3">{{ $team }}</h3>
        <p class="mb-3"><a style="text-decoration: underline;"
           href="{{ route('locations.ratings.show', $location) }}">
            @lang('Open city rating')
        </a></p>
        <p>@lang('Date of game'):
            <span class="h4 ml-3">
                @isset($location->timezone) @php
                    $session->played_at = $session->played_at->setTimezone($location->timezone);
                @endphp @endisset
                @if (app()->getLocale() == 'ru')
                    {{ $session->played_at->format('d.m.Y') }} г.
                @else
                    on {{ $session->played_at->isoFormat('Do MMM Y') }}
                @endif
            </span></p>
        <div class="row">
            <div class="table-responsive col-md-7">
                <table class="table table-borderless">
                    <thead>
                        <tr>
                            <th></th>
                            <th>@lang('Killed<br>zombies')</th>
                            <th>@lang('Headshots')</th>
                            <th>@lang('Killed<br>friends')</th>
                            <th>@lang('Deaths<br>qty')</th>
                            <th>@lang('Score')</th>
                        </tr>
                    </thead>
                    @foreach ($players as $player)
                        <tr>
                            <td class="rating-block-clip"><a>{{ $player->name }}</a></td>
                            <td>{{ $player->statistics->kills }}</td>
                            <td>{{ $player->statistics->headshots }}</td>
                            <td>{{ $player->statistics->team_kills }}</td>
                            <td>{{ $player->statistics->deaths }}</td>
                            <td>{{ $player->statistics->score }}</td>
                        </tr>
                    @endforeach
                </table>
                <div class="small" style="padding-left: .75rem;">
                    <div class="d-inline-block mr-3">
                        <span class="mr-2">@lang('Share with friends')</span>
                        <span data-share="{{ route('sessions.show', ['game' => $game, 'session' => $session]) }}" data-title="Результаты моей игры" data-image="{{ $imagePath }}">
                            <a href="https://vk.com/share.php?url={{ url()->current() }}" target="_blank"><img src="{{ asset('img/share_vk.svg') }}"" style="width: 1em;"></a>
                        </span>
                        <div class="fb-share-button"
                            data-href="{{ url()->current() }}"
                            data-title="Результаты моей игры"
                            data-layout="button"
                            data-image="{{ $imagePath }}"
                        ></div>
                    </div>
                    @isset($video) <a href="{{ $videoPath }}" target="_blank" class="mr-3">@lang('Download video') <img src="{{ asset('img/download.svg') }}" style="width: 1em;"></a> @endisset
                    @isset($image) <a href="{{ $imagePath }}" target="_blank" download="{{ $image->title }}">@lang('Download photo') <img src="{{ asset('img/download.svg') }}" style="width: 1em;"></a> @endisset
                </div>
            </div>
            <div class="col-md-5">
                <img class="w-100" src="{{ $imagePath ?? asset('img/no_photo.png') }}">
            </div>
        </div>
    @endforeach
@endsection

@push('styles')
    <style type="text/css">
        .rating-block h3 {
            margin-bottom: 2rem;
        }
        .rating-block {
            padding: 2rem;
        }
    </style>
@endpush
