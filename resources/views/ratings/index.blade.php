@extends('layouts.board')

@section('rating')
<div class="row">
    <h3 class="col-sm-8">{{ $title }}</h3>
    <form class="col-sm-4 rating-search">
        <input type="text" name="query" is="{{ $ratingEntity }}-search" @isset($location) location="{{ $location->slug }}" @endisset @isset($game) game="{{ $game->slug }}" @endisset>
    </form>
</div>
<div class="row">
    <div class="col-sm-4">
        @isset($location)
            <a class="nav-link" style="text-decoration: underline; padding-left: .125rem;" href="{{ route($routes['world']['name'], request()->only('period')) }}">
                {{ $routes['world']['title'] }}
            </a>
        @else
            @inject('locations', 'App\Services\LocationService')
            <a class="nav-link dropdown-toggle" style="text-decoration: underline; padding-left: .125rem;" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                {{ $routes['location']['title'] }}
            </a>
            <div class="dropdown-menu">
                @foreach ($locations->getActive() as $dropdownItem)
                    <a class="dropdown-item" href="{{ route($routes['location']['name'], array_merge(['location' => $dropdownItem], request()->only('period'))) }}">
                        {{ $dropdownItem->title }}
                    </a>
                @endforeach
            </div>
        @endisset
    </div>
    <div class="col-sm-4">
        @inject('statisticService', 'App\Services\StatisticService')
        <a class="nav-link dropdown-toggle" style="text-decoration: underline; padding-left: .125rem;" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
            {{ $statisticService->getCurrentPeriod() }}
        </a>
        <div class="dropdown-menu">
            @foreach ($statisticService->getPeriods() as $periodKey => $periodName)
                @continue($statisticService->isCurrent($periodKey))
                <a class="dropdown-item" href="{{ request()->fullUrlWithQuery(['period' => $periodKey]) }}">
                    {{ $periodName }}
                </a>
            @endforeach
        </div>
    </div>
    <div class="col-sm-4">
        @php
            $alternativeParams = array_merge(request()->only('period'), ['location' => $location]);
        @endphp
        <a class="nav-link" style="text-decoration: underline; padding-left: .125rem;" href="{{ route($routes['alternative']['name'], $alternativeParams) }}">
            {{ $routes['alternative']['title'] }}
        </a>
    </div>
</div>
<div class="table-responsive">
    <table class="table table-borderless">
        @php
            $previousPlace = $sessions->firstItem() - 1;
            $showPlace = $showPlace ?? true;
        @endphp

        <thead>
            <tr>
                <th></th>
                <th>@lang('Killed<br>zombies')</th>
                <th>@lang('Headshots')</th>
                <th>@lang('Killed<br>friends')</th>
                <th>@lang('Deaths<br>qty')</th>
                <th>@lang('Total<br>score')</th>
                @if ($showPlace) <th class="ranking-place">@lang('Place<br>in&nbsp;ranking')</th> @endif
            </tr>
        </thead>

        <tbody is="{{ $ratingEntity }}-index">
        @foreach($sessions as $session)
            <tr>
                <td class="rating-block-clip">
                    <a href="{{ route('sessions.show', ['game' => $game, 'session' => $session]) }}">{{ $session->title ?? $session->team->title ?? 'N/A' }}</a>
                    @if (!isset($location) && isset($session->location))<div class="small">{{ $session->location->title }}</div>@endif
                </td>
                <td>{{ $session->total_kills }}</td>
                <td>{{ $session->total_headshots }}</td>
                <td>{{ $session->total_team_kills }}</td>
                <td>{{ $session->total_deaths }}</td>
                <td>{{ $session->total_score }}</td>
                @if ($showPlace) <td>{{ $session->place ?? $previousPlace + $loop->iteration }}</td> @endif
            </tr>
        @endforeach
        </tbody>
        <tfoot>
            <tr><td colspan="{{ 5 + $showPlace }}">{{ $sessions->links('partials.pagination') }}</td></tr>
        </tfoot>
    </table>
</div>
@endsection

@push('styles')
<style type="text/css">
@verbatim
    .rating-block thead th {
        position: -webkit-sticky;
        position: sticky;
        top: 0;
        background-color: #1b276c;
    }
    @media (max-width: 575.98px) {
        .rating-block {
            padding: 1rem;
        }
    }
    @media (min-width: 576px) {
        .rating-block {
            padding: 1.75rem 2rem;
        }
    }
    @media (min-width: 768px) {
        .rating-block {
            padding: 2.5rem 3rem;
        }
    }
    @media (min-width: 992px) {
        .rating-block {
            padding: 3.75rem 6rem;
        }
    }
    @media (min-width: 1200px) {
        .rating-block {
            padding: 6rem 10.75rem;
        }
    }
@endverbatim
</style>
@endpush
