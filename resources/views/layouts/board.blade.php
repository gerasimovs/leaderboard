<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta property="fb:app_id" content="283111552325505" />
    <meta property="og:url" content="{{ url()->current() }}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="@yield('og:title', config('app.name', 'Laravel'))" />
    <meta property="og:description" content="Новый формат развлечений для тебя и твоих друзей. Авторская игра с полным погружением в виртуальную реальность для команды из 4 человек на площадке в 80 м2" />
    <meta property="og:image" content="@yield('og:image', asset('img/no_photo.png'))">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->

    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
    <style type="text/css">
        {{-- #app {
            background-image: url('{{ asset('img/bg_main.jpg') }}');
        } --}}
        .rating-block {
            background-image: url('{{ asset('img/bg_rating.jpg') }}');
        }
        .rating-search:before {
            background-image: url('{{ asset('img/input_search.svg') }}');
        }
    </style>
    @stack('styles')
</head>
<body>
    <div id="app" class="rating-wrapper">
        <nav>
            <ul class="nav rating-topmenu">
                <li class="nav-item">
                    <a class="navbar-brand" href="{{ url('/') }}">
                        <img src="{{ asset('img/header_logo.svg') }}" title="{{ config('app.name', 'Laravel') }}" style="max-height: 4rem;">
                    </a>
                </li>
                <li class="nav-item dropdown">
                    @inject('languageService', 'App\Services\LanguageService')
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                        <img src="{{ asset($languageService->getCurrent()['flag']) }}" style="height: 1rem; margin-right: .2rem;">
                    </a>
                    <div class="dropdown-menu">
                        @foreach($languageService->getItems() as $language)
                            @continue($language['is_active'])
    						<a class="dropdown-item" href="{{ request()->fullUrlWithQuery(['lang' => $language['abbr']]) }}">
    							<img src="{{ asset($language['flag']) }}" style="height: 1rem; margin-right: .2rem;">
                                {{ $language['title'] }}
    						</a>
                        @endforeach
                    </div>
                </li>
				<li class="nav-item"></li>
            </ul>
        </nav>

        <main class="rating-wrapper py-4">
            <div class="container rating-block">
                @yield('rating')
            </div>
        </main>

        <footer class="rating-footer">
            @inject('locations', 'App\Services\LocationService')
            <a href="https://thedeepvr.ru/" class="mb-3">
                <img src="{{ asset('img/footer_logo.svg') }}" title="{{ config('app.name', 'Laravel') }}" style="max-height: 6rem;">
            </a>
            @if ($locations->getWithSite()->isEmpty())
                <a href="https://thedeepvr.ru/">
                    <div class="btn">@lang('To Home Page')</div>
                </a>
            @else
                <div class="dropdown">
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="locationsMap" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">@lang('To Home Page')</button>
                    <div class="dropdown-menu" aria-labelledby="locationsMap">
                        @foreach ($locations->getWithSite() as $location)
                            <a class="dropdown-item" href="{{ $location->url }}" target="_blank">{{ $location->title }}</a>
                        @endforeach
                    </div>
                </div>
            @endif
        </footer>
    </div>
    <script src="{{ mix('js/app.js') }}"></script>
    <script src="https://vk.com/js/api/share.js?95" charset="windows-1251"></script>
    <script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0";
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));</script>
    <script>
        document.addEventListener('DOMContentLoaded', function () {
            document.querySelectorAll('[data-share]').forEach(function (el) {
                let shareButton = VK.Share.button({
                    url: el.dataset.share,
                    title: el.dataset.title || '{{ config('app.name') }}',
                    image: el.dataset.image,
                    noparse: true
                }, {
                    type: "custom",
                    text: "<img src=\"{{ asset('img/share_vk.svg') }}\" style=\"height: 1.25em;\" />"
                });

                el.innerHTML = shareButton;
            });
        });
    </script>
</body>
</html>
