<footer class="rating-footer">
    <div class="container">
        <div class="row">
            <div class="col-md-3 mb-5">
                <a href="https://thedeepvr.ru/">
                    <img src="{{ asset('img/footer_logo.svg') }}" title="{{ config('app.name', 'Laravel') }}" style="max-height: 6rem;">
                </a>
                <div class="mt-3">
                    @include('partials.social')
                </div>
                <div class="mt-3" style="color: #525252;">&copy; {{ date('Y') }} The Deep VR</div>
            </div>
            <div class="col-md-6 mb-5">
                <h3>Разделы сайта</h3>
                <div class="row">
                    <ul class="col-md-6 mb-0">
                        <li><a href="https://thedeepvr.ru/about">О проекте</a></li>
                        <li><a href="https://thedeepvr.ru/games_nsk">Наши игры</a></li>
                        <li><a href="https://thedeepvr.ru/certificate">Подарочный сертификат</a></li>
                        <li><a href="https://ru.thedeepvr.com/" target="_blank">Франшиза</a></li>
                    </ul>
                    <ul class="col-md-6">
                        <li><a href="https://thedeepvr.ru/payment">Оплата</a></li>
                        <li><a href="https://thedeepvr.ru/personal_data_policy">Политика персональных данных</a></li>
                        <li><a href="https://thedeepvr.ru/details">Реквизиты</a></li>
                        <li><a href="https://thedeepvr.ru/agreement">Пользовательское соглашение</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-3 mb-5">
                <h3>Контакты</h3>
                <div class="row">
                    <ul class="col-12">
                        <li>+7 (999) 467-10-10</li>
                        <li>nskthedeepvr@gmail.com</li>
                        <li>Время работы: ежедневно 10:00-23:00</li>
                        <li>Адрес: Красный проспект 101, <br>ТРК Ройял Парк</li>
                    </ul>
                </div>
            </div>
        </div>
        <div>
            <a href="http://nsk.mir-kvestov.ru/quests/questquest-safe-night" target="_blank"><img src="http://nsk.mir-kvestov.ru/widgets/4742/img" width="210" alt="Отзывы на Квест в реальности Safe Night (The Deep VR)" title="Отзывы на Квест в реальности Safe Night (The Deep VR)"></a>
        </div>
    </div>
</footer>

@push('styles')
<style type="text/css">
    .rating-footer li, .rating-footer a {
        color: #fff;
    }
    .rating-footer h3 {
        color: #525252;
        font-size: .85rem;
        letter-spacing: 1px;
        text-transform: uppercase;
        margin-bottom: 14px;
    }
    .rating-footer ul {
        list-style: none;
    }
    .rating-footer li {
        margin-bottom: 10px;
    }
</style>
@endpush
