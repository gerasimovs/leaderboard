@foreach (config('admin.social.novosibirsk') as $social => $link)
    <span @if (!$loop->first) class="ml-1" @endif>
        <a href="{{ $link }}" target="_blank"><img src="{{ asset('img/footer_' . $social . '.svg') }}"></a>
    </span>
@endforeach
