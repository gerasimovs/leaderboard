require('./bootstrap');

import Vue from 'vue'
import Buefy from 'buefy'

Vue.use(Buefy, {
    defaultIconPack: "fas"
});

import LocationsList from './components/Locations/List';
Vue.component('locations-list', LocationsList);

import GamesList from './components/Games/List';
Vue.component('games-list', GamesList);

const app = new Vue({
    data: function () {
        return {
            tabs: []
        }
    },
    methods: {
        deleteEntity: async function (event) {
            this.$dialog.confirm({
                title: 'Удаление',
                message: 'Вы действительно хотите удалить?',
                type: 'is-danger',
                hasIcon: true,
                onConfirm: () => {
                    let target = event.target;
                    if (!target.hasOwnProperty('href')) {
                        target = target.closest('[href]');
                    }

                    const form = document.createElement('form');
                    form.action = target.href;
                    form.method = 'post';

                    const csrf = document.createElement('input');
                    csrf.name = '_token';
                    csrf.type = 'hidden';
                    csrf.value = document.head.querySelector('meta[name="csrf-token"]').content;
                    form.appendChild(csrf);

                    const method = document.createElement('input');
                    method.name = '_method';
                    method.type = 'hidden';
                    method.value = 'DELETE';
                    form.appendChild(method);

                    document.body.appendChild(form);
                    form.submit();
                },
            });
        },
        copyToClipboard: function (text) {
            let dummy = document.createElement('textarea');
            document.body.appendChild(dummy);
            dummy.value = text;
            dummy.select();
            document.execCommand('copy');
            document.body.removeChild(dummy);

            this.$dialog.alert({
                title: 'Link copied!',
                message: 'Link <a href="' + text + '" target="_blank">' + text + '</a> copied to clipboard',
                size: 'is-small',
                confirmText: 'Ок',
                type: 'is-success',
                hasIcon: true,
                icon: 'check',
                iconPack: 'fas',
            })
        }
    }
});

const dashboardMain = document.getElementById('app');

if (dashboardMain) {
    app.$mount(dashboardMain);
}

document.addEventListener('DOMContentLoaded', () => {
    document.querySelectorAll('.navbar-burger').forEach( el => {
        el.addEventListener('click', () => {
            const target = el.dataset.target;
            const $target = document.getElementById(target);
            el.classList.toggle('is-active');
            $target.classList.toggle('is-hidden-mobile');
        });
    });
});
