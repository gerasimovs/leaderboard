const SET_SEARCH_QUERY = 'SET_SEARCH_QUERY';
const SET_LOADING = 'SET_LOADING';
const SET_ITEMS = 'SET_ITEMS';
const RESET_ITEMS = 'RESET_ITEMS';

const state = {
    loading: false,
    query: '',
    items: null,
};

export default {
    state,
    mutations: {
        [SET_SEARCH_QUERY]: (state, query) => state.query = query,
        [SET_LOADING]: (state, loading) => state.loading = loading,
        [SET_ITEMS]: (state, items) => {
            state.items = items;
            document.querySelector('th.ranking-place').style.display = 'none';
        },
        [RESET_ITEMS]: state => {
            state.items = null;
            document.querySelector('th.ranking-place').style.display = 'table-cell';
        },
    },
    actions: {
        setQuery({commit}, query) {
            commit(SET_SEARCH_QUERY, query);
        },
        async search({commit, state}, params) {
            if (!state.query) {
                commit(RESET_ITEMS);
                return;
            }

            if (typeof params !== 'object') {
                params = {};
            }

            params.query = state.query;

            commit(SET_LOADING, true);

            try {
                const {data} = await axios.get('/api/teams/', {params});
                commit(SET_ITEMS, data.data);
            } catch (e) {
                commit(RESET_ITEMS);
            }

            commit(SET_LOADING, false);
        }
  }
};
